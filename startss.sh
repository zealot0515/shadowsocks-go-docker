#!/bin/bash
if [ -f "/root/ss/ufw_rules.sh" ]; then
    ufw reset
    ufw default deny
    ufw allow 8888
    /bin/bash /root/ss/ufw_rules.sh
    ufw enable
fi
/root/ss/shadowsocks2-linux  -s "ss://${ENCRYPT_TYPE}:${SHADOWSOCKS_PASS}@:8888"