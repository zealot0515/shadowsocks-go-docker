# shadowsocks-go-docker

## 簡介   
把go-shadowsocks2與ufw打包成dockerimage
方便在一台vpn機器開多個shadowsockets給予不同的存取權

## ufw_rules.sh
把對應的ufw rule指令加入即可, 8888為必備(ssvpn使用)
```bash
ufw allow 8888
ufw allow to xxxxxxx
ufw deny to xxxxx
.....
```

## 環境變數
```
ENV ENCRYPT_TYPE 加密模式
ENV SHADOWSOCKS_PASS "密碼"
EXPOSE 8888
```
透過docker-compose帶入就可以改變