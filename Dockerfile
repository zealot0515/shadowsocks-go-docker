FROM ubuntu:18.04

RUN apt-get update
# Install service
RUN apt-get install -y wget ufw
RUN mkdir /root/ss/ && cd /root/ss/ && wget https://github.com/shadowsocks/go-shadowsocks2/releases/download/v0.0.11/shadowsocks2-linux.gz
RUN cd /root/ss/ && gzip -d shadowsocks2-linux.gz && chmod +x shadowsocks2-linux

ADD startss.sh /root/ss/

ENV ENCRYPT_TYPE AEAD_AES_256_GCM
ENV SHADOWSOCKS_PASS "THIS_IS_TEST_PASSWORD"
EXPOSE 8888

RUN chmod 700 /root/ss/startss.sh

ENTRYPOINT ["/root/ss/startss.sh"]
